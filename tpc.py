#! /usr/bin/env python

import urllib
verbose = True

class pwdURLopener(urllib.FancyURLopener):
    def prompt_user_passwd(self, host, realm):
        if realm == "inflate":
            return ("huge", "file")
        elif realm == "pluses and minuses":
            return ("butter", "fly")


def challenge_00():
    # http://www.pythonchallenge.com/pc/def/0.html
    print pow(2, 38)


def challenge_01():
    # www.pythonchallenge.com/pc/def/274877906944.html
    ciphertext = ("g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq "
                  "ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw "
                  "rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq "
                  "pcamkkclbcb. lmu ynnjw ml rfc spj. ")
    ciphertext2 = "map"
    plaintext = plaintext2 = ""
    for char in ciphertext:
        if ord(char) >= 97 and ord(char) <= 122:
            if ord(char) + 2 <= 122:
                plaintext += chr(ord(char) + 2)
            else:
                plaintext += chr(ord(char) + 1 - 122 + 97)
        else:
            plaintext += char
    if verbose: print plaintext
    for char in ciphertext2:
        if ord(char) >= 97 and ord(char) <= 122:
            if ord(char) + 2 <= 122:
                plaintext2 += chr(ord(char) + 2)
            else:
                plaintext2 += chr(ord(char) + 1 - 122 + 97)
        else:
            plaintext2 += char
    print plaintext2


def challenge_02():
    # http://www.pythonchallenge.com/pc/def/ocr.html
    import re
    urlfile = urllib.urlopen("http://www.pythonchallenge.com/pc/def/ocr.html")
    regexp = "<!--([\s\S]*?)-->"
    text = "".join(re.findall(regexp, urlfile.read())[1]).replace("\n", "")
    dict = {}
    for char in text:
        if char in dict:
            dict[char] += 1
        else:
            dict[char] = 1
    outstr = ""
    for char in text:
        if dict[char] == 1:
            outstr += char
    print outstr


def challenge_03():
    # http://www.pythonchallenge.com/pc/def/equality.html
    import re
    url = "http://www.pythonchallenge.com/pc/def/equality.html"
    urlfile = urllib.urlopen(url)
    regexp = "<!--([\s\S]*?)-->"
    lines = "".join(re.findall(regexp, urlfile.read())[0]).replace("\n", "")
    three_caps_regexp = "[^A-Z][A-Z][A-Z][A-Z]([a-z])[A-Z][A-Z][A-Z][^A-Z]"
    print "".join(re.findall(three_caps_regexp, lines))


def challenge_04():
    # http://www.pythonchallenge.com/pc/def/linkedlist.php
    import re
    pattern = re.compile("and the next nothing is ([0-9]+)$")
    url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing="
    digits = "12345"
    found = True
    while found:
        website_text = urllib.urlopen(url + digits).read()
        if verbose: print website_text
        maybe_match = pattern.search(website_text)
        if maybe_match is not None:
            digits = maybe_match.groups()[0]
        else:
            found = False
    digits = str(int(digits) / 2)
    found = True
    while found:
        website_text = urllib.urlopen(url + digits).read()
        if verbose: print website_text
        maybe_match = pattern.search(website_text)
        if maybe_match is not None:
            digits = maybe_match.groups()[0]
        else:
            found = False
    print website_text


def challenge_05():
    # http://www.pythonchallenge.com/pc/def/peak.html
    import pickle
    url = "http://www.pythonchallenge.com/pc/def/banner.p"
    unpickled = pickle.load(urllib.urlopen(url))
    for line in unpickled:
        print "".join(map(lambda (chr, times): chr * times, line))

    
def challenge_06():
    # http://www.pythonchallenge.com/pc/def/channel.html
    import re, zipfile, StringIO
    url = urllib.urlopen("http://www.pythonchallenge.com/pc/def/channel.zip")
    zip_file = zipfile.ZipFile(StringIO.StringIO(url.read()))
    if verbose: print zip_file.open("readme.txt").read()
    pattern = re.compile("nothing is ([0-9]+)$")
    digits = "90052"
    found = True
    outstr = ""
    while found:
        outstr += zip_file.getinfo(digits + ".txt").comment
        text = zip_file.open(digits + ".txt").read()
        if verbose: print text
        maybe_match = pattern.search(text)
        if maybe_match is not None:
            digits = maybe_match.groups()[0]
        else:
            found = False
    print outstr


def challenge_07():
    # http://www.pythonchallenge.com/pc/def/oxygen.html
    from PIL import Image
    import re, StringIO
    url = "http://www.pythonchallenge.com/pc/def/oxygen.png"
    img = urllib.urlopen(url).read()
    image = Image.open(StringIO.StringIO(img))
    pixels = list(image.getdata())
    width = image.size[0]
    pixels_by_line = []
    while pixels:
        pixels_by_line.append(pixels[:width])
        pixels = pixels[width:]
    greys = filter(lambda line: line[0][0] == line[0][1] == line[0][2],
                                pixels_by_line)[0]
    digits = [r for (r, g, b, t) in greys][:-21]
    chars = "".join(map(lambda x: chr(x), digits))
    if verbose: print chars
    pattern = re.compile("(\d+)(,+ +)?")
    str = map(lambda (digits, maybe_delim): digits, pattern.findall(chars))
    outstr = []
    for sevenfold in str:
        remove_sevenfolds = [sevenfold[i] for i in range(0, len(sevenfold), 7)]
        outstr.append(int("".join(remove_sevenfolds)))
    print "".join(map(lambda x: chr(x), outstr))


def challenge_08():
    # http://www.pythonchallenge.com/pc/def/integrity.html
    import bz2
    user_name = ("BZh91AY&SYA\xaf\x82\r\x00\x00\x01\x01\x80\x02\xc0\x02\x00 "
                 "\x00!\x9ah3M\x07<]\xc9\x14\xe1BA\x06\xbe\x084")
    password = ("BZh91AY&SY\x94$|\x0e\x00\x00\x00\x81\x00\x03$ "
                "\x00!\x9ah3M\x13<]\xc9\x14\xe1BBP\x91\xf08")
    print "Username: " + bz2.decompress(user_name.decode("string-escape"))
    print "Password: " + bz2.decompress(password.decode("string-escape"))


def challenge_09():
    # http://www.pythonchallenge.com/pc/return/good.html
    import re, pygame
    url = "http://www.pythonchallenge.com/pc/return/good.html"
    html = pwdURLopener().open(url).read()
    first_regexp = re.compile("first:\n(.*)second:", re.DOTALL)
    first = first_regexp.findall(html)[0][:-2].replace("\n", "").split(",")
    second_regexp = re.compile("second:\n(.*)--", re.DOTALL)
    second = second_regexp.findall(html)[0][:-2].replace("\n", "").split(",")
    pairs_1 = zip(map(int, first[::2]), map(int, first[1::2]))
    pairs_2 = zip(map(int, second[::2]), map(int, second[1::2]))
    pygame.init()
    width = max(map(lambda (x, y): x, pairs_1 + pairs_2))
    height =  max(map(lambda (x, y): y, pairs_1 + pairs_2))
    screen = pygame.display.set_mode([width, height])
    clock = pygame.time.Clock()
    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
        screen.fill([255, 255, 255])
        pygame.draw.polygon(screen, [255, 0, 0], pairs_1)
        pygame.draw.polygon(screen, [0, 0, 255], pairs_2)
        clock.tick(20)
        pygame.display.flip()
    pygame.quit()


def challenge_10():
    # http://www.pythonchallenge.com/pc/return/bull.html
    morris_sequence = ["1"]
    for i in range(31):
        morris = morris_sequence[i]
        next_morris = ""
        count = 1
        last_char = morris[0]
        for char in morris[1:]:
            if char == last_char:
                count += 1
            else:
                next_morris += (str(count) + str(last_char))
                last_char = char
                count = 1
        next_morris += (str(count) + last_char)
        morris_sequence.append(next_morris)
    print len(morris_sequence[30])


def challenge_11():
    # http://www.pythonchallenge.com/pc/return/5808.html
    from PIL import Image
    import StringIO
    url = "http://www.pythonchallenge.com/pc/return/cave.jpg"
    image = Image.open(StringIO.StringIO(pwdURLopener().open(url).read()))
    even_image = Image.new("RGB", image.size)
    odd_image = Image.new("RGB", image.size)
    pixels = image.getdata()
    width, height = image.size
    even_x = even_y = odd_x = odd_y = 0
    for i in range(len(pixels)):
        if i%2 == 0:
            even_image.putpixel((even_x, even_y), pixels[i])
            even_x += 1
        else:
            odd_image.putpixel((odd_x, odd_y), pixels[i])
            odd_x += 1
        if even_x == width:
            even_x = 0
            even_y += 1
        elif odd_x == width:
            odd_x = 0
            odd_y += 1
    even_image.show()
    odd_image.show()


def challenge_12():
    # http://www.pythonchallenge.com/pc/return/evil.html
    from PIL import Image
    import StringIO, os
    url = "http://www.pythonchallenge.com/pc/return/evil2.gfx"
    gfx_file = pwdURLopener().open(url).read()
    deal_five = [gfx_file[i::5] for i in range(5)]
    for i in range(len(deal_five)):
        try:
            Image.open(StringIO.StringIO(deal_five[i])).show()
        except IndexError:
            print ("image %d is corrupted... saving to %s"
                   % (i, os.path.realpath(__file__) + "\\tmp_%d" % i))
            open("tmp_%d" % i, "wb").write(deal_five[i])


def challenge_13():
    # http://www.pythonchallenge.com/pc/return/disproportional.html
    import re, xmlrpclib
    url = "http://www.pythonchallenge.com/pc/return/evil4.jpg"
    html = pwdURLopener().open(url).read()
    evil = re.compile("(.*) is evil").findall(html)[0]
    url_phonebook = "http://www.pythonchallenge.com/pc/phonebook.php"
    server = xmlrpclib.ServerProxy(url_phonebook)
    for method in server.system.listMethods():
        if verbose: print method, server.system.methodHelp(method)
    print re.compile("555-([A-Z]+)").findall(server.phone(evil))[0].lower()


def challenge_14(): # http://www.pythonchallenge.com/pc/return/italy.html
    from PIL import Image
    import StringIO
    url = "http://www.pythonchallenge.com/pc/return/wire.png"
    image = Image.open(StringIO.StringIO(pwdURLopener().open(url).read()))
    pixels = image.getdata()
    out_img = Image.new("RGB", [100, 100])
    (x, y) = (0, 0)
    put_in_same_direction = 100
    second_run = True
    moves_index = pixels_index = 0
    moves = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    while pixels_index < len(pixels) - 1:
        for i in range(put_in_same_direction if put_in_same_direction != 100
                                             else put_in_same_direction - 1):
            out_img.putpixel((x, y), pixels[pixels_index])
            (x, y) = (x + moves[moves_index % 4][0],
                      y + moves[moves_index % 4][1])
            pixels_index += 1
        if second_run:
            put_in_same_direction -= 1
            second_run = False
        else:
            second_run = True
        moves_index += 1
    out_img.show()


def challenge_15():
    # http://www.pythonchallenge.com/pc/return/uzi.html
    def is_leapyear(y):
        return y % 4 == 0 and (y % 100 != 0 or (y % 100 == 0 and y % 400 == 0))
    
    import calendar
    cal = calendar.Calendar()
    days = {0:"Monday", 1:"Tuesday", 2:"Wednesday", 3:"Thursday", 4:"Friday",
            5:"Saturday", 6:"Sunday"}
    years = filter(lambda y: is_leapyear(y), range(1006, 1996 + 1)[::10])
    candidates = []
    for year in years:
        month = [day for month in cal.yeardays2calendar(year, 1)[0]
                     for week in month for day in week]
        for (day_num, day_name) in month:
            if day_num == 26 and days[day_name] == "Monday":
                candidates.append((days[day_name], day_num, "January", year))
    print "%s %d %s %d" % (candidates[-2][0], candidates[-2][1] + 1,
                           candidates[-2][2], candidates[-2][3])


def challenge_16():
    # http://www.pythonchallenge.com/pc/return/mozart.html
    from PIL import Image
    import StringIO
    url = "http://www.pythonchallenge.com/pc/return/mozart.gif"
    image = Image.open(StringIO.StringIO(pwdURLopener().open(url).read()))
    width, height = image.size
    first_line = [image.getpixel((x, 1)) for x in range(width)]
    pink, i = None, 0
    while not pink and i < len(first_line):
        if [first_line[i]] * 5 == first_line[i:i + 5]:
            pink = first_line[i]
        else:
            i += 1
    for y in range(height):
        line = [image.getpixel((x, y)) for x in range(width)]
        pink_start = 0
        while line[pink_start] != pink:
            pink_start += 1
        line = line[pink_start:] + line[:pink_start]
        for x in range(len(line)):
            image.putpixel((x, y), line[x])
    image.show()


def challenge_17():
    # http://www.pythonchallenge.com/pc/return/romance.html
    import re, urllib2, cookielib, bz2, xmlrpclib
    cookie_jar = cookielib.CookieJar()
    auth_handler = urllib2.HTTPBasicAuthHandler()
    cookie_handler = urllib2.HTTPCookieProcessor(cookie_jar)
    auth_handler.add_password("inflate", "www.pythonchallenge.com", "huge",
                              "file")
    opener = urllib2.build_opener(auth_handler, cookie_handler)
    url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?busynothing="
    next_nothing = "12345"
    pattern = re.compile("and the next busynothing is ([0-9]+)$")
    found, cookie_values = True, []
    while found:
        website_text = opener.open(url + next_nothing).read()
        if verbose: print website_text
        maybe_match = pattern.search(website_text)
        if maybe_match is not None:
            next_nothing = maybe_match.groups()[0]
            cookie_values.append(list(cookie_jar)[0].value)
        else:
            found = False
    if verbose:
        cookie_str = "".join(cookie_values)
        print bz2.BZ2Decompressor().decompress(urllib.unquote_plus(cookie_str))
    message = urllib.quote_plus("the flowers are on their way")
    if verbose:
        phonebook_url = "http://www.pythonchallenge.com/pc/phonebook.php"
        print xmlrpclib.ServerProxy(phonebook_url).phone("Leopold")
    violin_url = "http://www.pythonchallenge.com/pc/stuff/violin.php"
    request = urllib2.Request(violin_url, headers={"Cookie": "info=" + message})
    print urllib2.urlopen(request).read()


def challenge_18():
    # http://www.pythonchallenge.com/pc/return/balloons.html
    from PIL import Image
    import StringIO, gzip, difflib
    url = "http://www.pythonchallenge.com/pc/return/deltas.gz"
    res = pwdURLopener().open(url).read()
    gzip_content_str = gzip.GzipFile(fileobj=StringIO.StringIO(res)).read()
    gzip_content = gzip_content_str.split("\n")
    delim = "   "
    end = gzip_content[0].find(delim)
    left = [line[:end] for line in gzip_content]
    right = [line[end + len(delim):] for line in gzip_content]
    diff = difflib.ndiff(left, right)
    diff_results = {"-":"", "+":"", " ":""}
    for elem in diff:
        diff_results[elem[0]] += "".join([chr(int(bit, 16))
                                          for bit in elem[2:].split()])
    for (key, value) in diff_results.iteritems():
        Image.open(StringIO.StringIO(value)).show()


def challenge_19():
    # http://www.pythonchallenge.com/pc/hex/bin.html
    pass


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        i = 1
        while i < len(sys.argv):
            param = sys.argv[i]
            if param == "--help" or param == "-h":
                print "Usage:\t%s [-h] [-q] [-c N]" % sys.argv[0]
                print "\nProvides solutions to The Python Challenge.\n"
                print "Optional arguments:"
                print "-h, --help\t\tShow this help message and exit."
                print "-q, --quiet\t\tDon't display intermediate results."
                print ("-c N, --challenge N\tSpecify challenge to compute "
                       "solution for. Default: most recently solved.")
            elif param == "--quiet" or param == "-q":
                verbose = False
            elif param == "--challenge" or param == "-c":
                if i == len(sys.argv)-1:
                    print ("%s flag should be followed by the number of "
                           "challenge to solve." % param)
                    break
                challenge_number = sys.argv[i + 1]
                if not challenge_number.isdigit():
                    print ("\"%s\" is not a valid challenge identifier. "
                           "Try positive integers." % challenge_number)
                    break
                if "challenge_" + challenge_number in locals():
                    print ("---------------Challenge %s:---------------\n"
                           % challenge_number)
                    locals()["challenge_" + challenge_number]()
                    i += 1
                    print "\n\n"
                else:
                    solved = [f for f in locals().keys()
                                if f[:-2] == "challenge_" and f[-2:].isdigit()]
                    print ("A solution to challenge %s has yet to be "
                           "implemented." % challenge_number)
                    print ("Challenges solved so far: %s"
                           % ", ".join(sorted([str[-2:] for str in solved])))
                    break
            else:
                print ("Unknown flag \"%s\". Try \"%s --help\" for "
                       "documentation." % (param, sys.argv[0]))
                break
            i += 1
    else:
        challenge = sorted([f for f in locals().keys()
                              if f[:-2] == "challenge_" and f[-2:].isdigit()],
                           reverse=True)[0]
        print "---------------Challenge %s:---------------\n" % challenge[-2:]
        locals()[challenge]()
